# Gitlab Adullact Stats

## Requirements

* Ruby 2.3.3
* A valid token for an admin user of the Gitlab to query

## Usage

From CLI:

* In `ditlab_stats.rb`, adjust `my_year`
* From shell, define `GITLAB_API_PRIVATE_TOKEN` with the value of the token
* Run `ruby gitlab_stats.rb

From RubyMine:

* Open the project
* Verify the chosen interpreter is of version 2.3.3: File > Settings > Languages & Frameworks > Ruby SDK and Gems
* Define env variable `GITLAB_API_PRIVATE_TOKEN` with the value of the token
  in Run > Edit configurations

## Resources

Gitlab API doc:

* [Gitlab API pagination](https://docs.gitlab.com/ce/api/README.html#pagination-link-header)
* [Users API](https://docs.gitlab.com/ce/api/users.html)
* [Project API](https://docs.gitlab.com/ce/api/projects.html)

Wrapper to Gitlab API:

* [Github NARKOZ/gitlab](https://github.com/NARKOZ/gitlab)
* [NARKOZ/gitlab doc](http://narkoz.github.io/gitlab)
* CURL

## Number of users (old version)

In Shell, first define:

* `GITLAB_PERSONAL_TOKEN` with the value of the token
* `YEAR` with the desired year on 4 digit (eg 2016)

```sh
curl -s --head --header "PRIVATE-TOKEN: ${GITLAB_PERSONAL_TOKEN}" "https://gitlab.adullact.net/api/v4/users?created_after=${YEAR}-01-01T00:00:00.060Z&created_before=${YEAR}-12-31T23:59:59.060Z" |grep "X-Total:"
```

## New version: number of users and projects created in a given year

--> use `gitlab_stats.rb`