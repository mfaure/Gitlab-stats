#
# /!\ don't forget to define the SHELL variable GITLAB_API_PRIVATE_TOKEN
#

require 'gitlab'
require 'date'

# set an API endpoint
Gitlab.endpoint = 'https://gitlab.adullact.net/api/v4'

# define year
my_year = 2017
year_day1 = Date.new(my_year).to_s
year_day366 = Date.new(my_year + 1).to_s

# list projects ================================================================
# Note: API ()or Ruby wrapper) does not support
#   `created_after` or `created_before` parameters for PROJECTS objects
projects = Gitlab.projects.auto_paginate

projects_of_year = projects.select do |prj|
  prj.created_at < year_day366
end

projects_of_year = projects_of_year.select do |prj|
  prj.created_at >= year_day1
end

projects_of_year.each do |prj|
  puts "Projet créé le #{prj.created_at} : #{prj.name}"
end

# list users ===================================================================
users_of_year = Gitlab
                    .users(created_after: year_day1,
                           created_before: year_day366)
                    .auto_paginate

users_of_year.each do |u|
  puts "Utilisateur créé le #{u.created_at} : #{u.name} - #{u.username}"
end

# print consolidated results for current year
puts "\n"
puts "Nombre de projets : #{projects_of_year.size}"
puts "Nombre d'utilisateurs : #{users_of_year.size}"